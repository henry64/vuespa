import { polyfill } from 'es6-promise'
polyfill()
import { mount } from 'avoriaz'
import { expect } from 'chai'
import shows from '@/components/showsComponent.vue'
import vue from 'vue'
import vuex from 'vuex'
import store from '@/store'


vue.use(vuex)


describe('showsComponent.vue', () => {
  it('renders an awesome title', () => {
    const wrapper = mount(shows, {
        store: store
    })
    const componentTitle = wrapper.find('show-title')
    expect(componentTitle.html).toBe('TV Show Store')
  })
})