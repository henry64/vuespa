import Vue from 'vue'
import Router from 'vue-router'
import Vuex from 'vuex'
import shows from '@/components/showsComponent'
import favs from '@/components/favShowsComponent'
import showdetails from '@/components/showDetailsComponent'

Vue.use(Router)
Vue.use(Vuex)

export default new Router({
  routes: [
    { path: '/', component: shows, alias: '/shows' },
    { path: '/favs', component: favs, alias: '/starred' },
    { path: '/showdetails/:idshow', component: showdetails, props: true }
  ]
})
