import Vue from 'vue'
import Vuex from 'vuex'

import api from '../api'
import * as getters from './getters'
import * as types from './mutation-types'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    shows: [],
    loading: false,
    error: false
  },
  getters,
  mutations: {
    [types.SHOWS_LOADING] (state) {
      state.loading = true
    },
    [types.SHOWS_LOADED] (state, shows) {
      state.shows = shows
      state.loading = false
    },
    [types.SHOWS_LOADING_ERROR] (state, error) {
      state.error = true
    }
  },
  actions: {
    async fetchAllShows ({ commit }) {
      commit('SHOWS_LOADING')
      let res = await api.fetchAllShowsApi()
      commit('SHOWS_LOADED', res.data)
    }
  }
})

export default store