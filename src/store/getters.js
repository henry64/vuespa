// Récupère le listing des TV Shows et les trie ASC sur title
const showListing = state => [...state.shows].sort((elem1, elem2) => elem1.title.localeCompare(elem2.title))

export {
    showListing
}