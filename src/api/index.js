import axios from 'axios'

export default {
    async fetchAllShowsApi () {
        return await axios.get('http://localhost:4000/rest/shows')
    }
}